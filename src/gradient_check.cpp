/*
    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#include <iostream>
#include <mpi.h>

#include "config.hpp"
#include "main.hpp"
#include "model_test.hpp"
#include "mpi_tools.hpp"
#include "sgp_dda.hpp"

inline void abort()
{
   PetscFinalize();
   MPI_Finalize();
   exit(1);
}

void plot_gradient(const InputArgs& args, Parameters& param, int edge_idx, int k1, int k2, int n)
{
   compute_coating_layer(args, param);
   auto alg = initialize_optimization<1>(args, param);

   auto check = sgp::gradient_check_S(*alg, edge_idx, k1, k2, n);

   double Ju = check[0].J;
   std::cout << "# rho(vi) J(u-ui+vi) S[u](vi)\n";

   std::cout.precision(16);
   for (auto& val : check)
   {
      val.J -= Ju; // substract Ju since it is also neglected in S[u]
      std::cout << val << std::endl;
   }
}

int main(int argc, char** argv)
{
   MPI_Init(&argc, &argv);
   PetscInitialize(&argc, &argv, nullptr, nullptr);

   Parameters param;
   MPI_Comm_rank(MPI_COMM_WORLD, &param.rank);
   MPI_Comm_size(MPI_COMM_WORLD, &param.np_world);

   bool err = false;
   if (argc < 2)
   {
      if (param.rank == 0)
         std::cout << "Usage: " << argv[0] << " PARAMETERFILE [edge_idx=0] [k1=0] [k2=0] [n=10]\n";
      abort();
   }
   if (param.np_world > 1)
   {
      if (param.rank == 0)
         std::cerr << "ERROR: gradient_check should not be called with more than 1 MPI process!\n";

      abort();
   }

   ////////////////////////////////////////////////////////
   // read parameters
   ////////////////////////////////////////////////////////

   auto args = read_arguments(param, argv[1]);

   read_data(args, param);

   if (args.interpolation != 1)
   {
      std::cerr << "Only linear interpolation is implemented -> n_en = 2.\n";
      abort();
   }

   int edge_idx = 0;
   int k1       = 0;
   int k2       = 0;
   int n        = 10;

   if (argc > 2)
   {
      edge_idx = std::atoi(argv[2]);
   }
   if (argc > 3)
   {
      k1 = std::atoi(argv[3]);
   }
   if (argc > 4)
   {
      k2 = std::atoi(argv[4]);
   }
   if (argc > 5)
   {
      n = std::atoi(argv[5]);
   }

   ////////////////////////////////////////////////////////
   // Do work
   ////////////////////////////////////////////////////////

   domain_decomposition(args, param);
   initialize_dda(args, param);
   plot_gradient(args, param, edge_idx, k1, k2, n);

   ////////////////////////////////////////////////////////
   // finalize
   ////////////////////////////////////////////////////////

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> PROGRAM FINISHED <<<<<<<<<<\n";
   }

   dda_free();
   PetscFinalize();
   MPI_Finalize();
   return 0;
}
