/*
    Specialized Class representing an SPG-algorithm using discrete dipole approximation (DDA)

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include "sgp_dda.hpp"

std::ostream& operator<<(std::ostream& out, const polarization_t& E)
{
   out << "[" << E.target_rotation[0] << ", " << E.target_rotation[1] << ", " << E.target_rotation[2] << "], [";
   out << E.jones_vector[0] << ", " << E.jones_vector[1] << "]";
   return out;
}

std::ostream& operator<<(std::ostream& out, const complex_3d& vec)
{
   for (const auto& el : vec)
   {
      out << el << " | ";
   }
   return out;
}

template <int INTERPOLATION>
SGP_DDA<INTERPOLATION>::SGP_DDA(const sgp::Arr1D<bool>                   to_optimize,
                                const sgp::Arr2D<complex_t>&             materials_,
                                const sgp::Arr1D<edge_t>&                edges_,
                                const sgp::Arr1D<wave_t>&                mods_1_,
                                const sgp::Arr1D<polarization_t>&        mods_2_,
                                const sgp::mpi::DomainDecomposition      dd_,
                                const std::shared_ptr<const PETScMatrix> F,
                                const double                             filter_penalty,
                                const bool                               verbose_,
                                const bool                               initial_guess_P_old)
: sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>::Algorithm(to_optimize,
                                                                                          materials_,
                                                                                          edges_,
                                                                                          mods_1_,
                                                                                          mods_2_,
                                                                                          dd_,
                                                                                          verbose_)
, _R(mods_1_.size())
, _filt_pen(filter_penalty)
, _initial_guess_P_old(initial_guess_P_old)
{
   // allocate mem for converting material configuration to OpenDDA datatype dcomplex
   _u_cpy = C_vec_dda(N);

   if (F)
   {
      // precompute intermediate matrix for filter term
      // F-I
      auto FmI = F->shiftDiag(-1);
      // (F-I)^T (F-I)
      auto M = FmI->AtA();

      // precompute diag(M)
      _diagM = M->diag();

      // precompute M-diag(M)
      PETScVector zero(dda_N_local(), dda_N_global(), dd_.dof.comm);
      _MmD = M->setDiag(zero);

      // allocate memory for PETSc Vectors
      _u_petsc = std::make_shared<PETScVector>(dda_N_local(), dda_N_global(), dd_.dof.comm);

      for (int i = 0; i < _R.size(); ++i)
      {
         _R[i] = std::make_shared<PETScVector>(dda_N_local(), dda_N_global(), dd_.dof.comm);
      }
   }
}

template <int INTERPOLATION>
void SGP_DDA<INTERPOLATION>::prepare_iteration()
{
   if (_MmD)
   {
      // precompute R = (M - diag(M)) * u_old
      for (int i = 0; i < _R.size(); ++i)
      {
         _u_petsc->bind(u_old[i]);
         _MmD->apply(*_u_petsc, *(_R[i]));
      }
   }
}

template <int INTERPOLATION>
inline void SGP_DDA<INTERPOLATION>::eval_generic(const sgp::Arr1D<complex_t>& u,
                                                 sgp::Arr1D<complex_3d>&      P,
                                                 const real_t&                wavelen,
                                                 const polarization_t&        pol,
                                                 const bool                   adjoint) const
{
   convert_material(u, _u_cpy);

   auto phi   = pol.target_rotation[0];
   auto theta = pol.target_rotation[1];
   auto psi   = pol.target_rotation[2];
   auto Ex    = to_dcomplex(pol.jones_vector[0]);
   auto Ey    = to_dcomplex(pol.jones_vector[1]);

   // setup A
   dda_set_incident_field(phi, theta, psi, Ex, Ey);
   dda_set_wavelength(wavelen);
   dda_setup_interaction_matrix(_u_cpy.data(), 0);

   // use old solution as initial guess
   if (_initial_guess_P_old)
      convert_solution(P, dda_get_initialGuessPtr());

   // solve for P
   dda_compute_P(adjoint);
   convert_solution(dda_getP(), P);
}

template <int INTERPOLATION>
inline double SGP_DDA<INTERPOLATION>::eval(const sgp::Arr1D<complex_t>& u,
                                           sgp::Arr1D<complex_3d>&      P,
                                           const wave_t&                wave,
                                           const polarization_t&        pol) const
{
   eval_generic(u, P, wave.wavelen, pol, false);

   // return J
   return dda_get_c_ext();
}

template <int INTERPOLATION>
inline void SGP_DDA<INTERPOLATION>::eval_adj(const sgp::Arr1D<complex_t>& u,
                                             sgp::Arr1D<complex_3d>&      Q,
                                             const wave_t&                wave,
                                             const polarization_t&        pol) const
{
   eval_generic(u, Q, wave.wavelen, pol, true);
}

template <int INTERPOLATION>
inline double SGP_DDA<INTERPOLATION>::S(const complex_t&  u_i,
                                        const complex_t&  v_i,
                                        const complex_3d& P_i,
                                        const complex_3d& Q_i,
                                        const double      J_pen_v_i,
                                        const wave_t&     wave,
                                        const polarization_t&,
                                        const double tau,
                                        const int    i,
                                        const int    k1,
                                        double&      J_filt) const
{
   complex_t a_u = alpha(u_i);
   complex_t a_v = alpha(v_i);

   // (a(v) - a(u)) / a(u)^2
   complex_t T_alpha = (a_v - a_u) / (a_u * a_u);

   // 4*pi*k
   double T_k = 8.0 * M_PI * M_PI / wave.wavelen;

   // J_filt = c [ v_i^* R_i + v_i R_i^* + M_ii|v_i|^2 ]
   J_filt = 0;
   if (_diagM) // ignore filter term if no filter matrix is specified
   {
      auto R_i  = _R[k1]->getValue_loc(i);
      auto M_ii = (_diagM->getValue_loc(i)).real();
      J_filt    = _filt_pen * (2 * (v_i.real() * R_i.real() + v_i.imag() * R_i.imag()) + M_ii * norm2(v_i));
   }

   // J_pen(vi) + tau*||ui - vi||^2 + J_filt
   double T_pen = J_pen_v_i + tau * std::norm(u_i - v_i) + J_filt;

   // Q*P
   complex_t QP = 0.0;

   for (size_t d = 0; d < 3; ++d)
   {
      QP += Q_i[d] * P_i[d];
   }

   // return s
   return T_k * std::imag(T_alpha * QP) + T_pen;
}

template <int INTERPOLATION>
inline double SGP_DDA<INTERPOLATION>::norm2(const complex_t& u_i) const
{
   return std::norm(u_i);
}

template <int INTERPOLATION>
double SGP_DDA<INTERPOLATION>::sumJ(const sgp::Arr2D<double>& J,
                                    const sgp::Arr1D<wave_t>& waves,
                                    const sgp::Arr1D<polarization_t>&) const
{
   double sum     = 0;
   double scaling = 0;

   for (size_t i = 0; i < J.rows(); ++i)
   {
      for (size_t j = 0; j < J.cols(); ++j)
      {
         sum += waves[i].weight * J[i][j];
      }
      scaling += std::abs(waves[i].weight) * J.cols();
   }

   return sum / scaling;
}

template <int INTERPOLATION>
double SGP_DDA<INTERPOLATION>::gradSumJ(const sgp::Arr2D<double>&,
                                        const sgp::Arr2D<double>& dJ,
                                        const sgp::Arr1D<wave_t>& waves,
                                        const sgp::Arr1D<polarization_t>&) const
{
   double sum     = 0;
   double scaling = 0;

   for (size_t i = 0; i < dJ.rows(); ++i)
   {
      for (size_t j = 0; j < dJ.cols(); ++j)
      {
         sum += waves[i].weight * dJ[i][j];
      }
      scaling += std::abs(waves[i].weight) * dJ.cols();
   }

   return sum / scaling;
}

// template specializations
template class SGP_DDA<1>;
// template class SGP_DDA<2>;
// template class SGP_DDA<3>;