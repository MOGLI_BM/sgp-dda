/*
    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#include "main.hpp"

#include <chrono>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mpi.h>
#include <thread>

#include "config.hpp"
#include "data.hpp"
#include "sgp_dda.hpp"
#include "vtk_writer.hpp"

std::vector<size_t> read_backup(const std::string& file, int rnk)
{
   auto      filename = file + std::to_string(rnk);
   const int N        = dda_N_local();

   auto u = tyfir::read_scalars<size_t>(filename);

   if (N != u.size())
   {
      throw std::runtime_error("INPUT ERROR: Backup file " + filename + " corrupted!\nNumber of entries = " +
                               std::to_string(u.size()) + " (should be " + std::to_string(N) + ")!");
   }

   return u;
}

void write_backup(const sgp::Arr1D<sgp::Result>& result, const std::string& file, int rnk)
{
   auto      filename = file + std::to_string(rnk);
   const int N        = result.size();

   std::ofstream backup(filename);

   for (auto& el : result)
   {
      backup << el.idx << "\n";
   }
}

template <int INTERPOLATION>
void show_results_properties(const InputArgs& args, Parameters& param, std::shared_ptr<SGP_DDA<INTERPOLATION>> alg)
{
   auto J = alg->getJvec();
   if (param.rank == 0)
   {
      // write to stdout
      if (args.verbose)
      {
         std::cout << "\n>>>>>>>>>> PROPERTIES OF FINAL CONFIGURATION <<<<<<<<<<\n";
         std::cout << "\n>>>> Extinction per wavelength and incident direction/polarization <<<<\n";
         std::cout << std::setw(15) << "wl";
         for (size_t j = 0; j < param.E.size(); ++j)
         {
            std::cout << std::setw(15) << "J(u*, wl, E_" << j << ")";
         }
         std::cout << "\n--------------------------------------------------\n";
         for (size_t i = 0; i < param.waves.size(); ++i)
         {
            std::cout << "\n" << std::setw(15) << param.waves[i].wavelen;

            for (size_t j = 0; j < param.E.size(); ++j)
            {
               std::cout << std::setw(17) << J[i][j];
            }
         }
         std::cout << "\n";
      }

      // write to csv-file
      if (not args.outfile_ext.empty())
      {
         std::ofstream ext_file(args.outfile_ext);
         ext_file.precision(16);

         ext_file << "# wavelength";
         for (size_t j = 0; j < param.E.size(); ++j)
         {
            ext_file << ", J(u*, wl, E_" << j << ")";
         }
         ext_file << "\n";

         for (size_t i = 0; i < param.waves.size(); ++i)
         {
            ext_file << param.waves[i].wavelen;
            for (size_t j = 0; j < param.E.size(); ++j)
            {
               ext_file << ", " << J[i][j];
            }
            ext_file << "\n";
         }
      }
   }
}

template <int INTERPOLATION>
void write_iter_info(const InputArgs& args, Parameters& param, std::shared_ptr<SGP_DDA<INTERPOLATION>> alg)
{
   std::vector<sgp::IterInfo> info = alg->get_iter_info();

   if (param.rank == 0 and not args.outfile_iter.empty())
   {
      std::ofstream iter_file(args.outfile_iter);
      iter_file.precision(16);

      iter_file << "# Iteration, J(u), J_pen(u), J_reg(u_new, u_old), delta_j, n_inner, tau, time\n";

      for (auto& iter : info)
      {
         iter_file << iter << "\n";
      }
   }
}

void vtk_output(const InputArgs& args, Parameters& param, const std::vector<sgp::Result>& u_grayness)
{
   const int N = dda_N_local();

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> WRITE VTK FILE <<<<<<<<<<\n\n";
   }

   // create data maps
   vtk::DataMap<int64_t> mat_idx;
   // vtk::DataMap<int64_t> edge_idx;
   vtk::DataMap<double> grayness;
   // vtk::DataMap<double> rho;

   vtk::DataMap<int> particle;

   const int* coords = dda_get_coordinates();

   std::set<size_t> planes;

   for (size_t i = 0; i < N; ++i)
   {
      vtk::Point3D<size_t> point;

      for (size_t j = 0; j < 3; ++j)
         point[j] = size_t(coords[3 * i + j]);

      planes.insert(point.z());

      particle[point] = 1;

      mat_idx[point]  = u_grayness[i].idx;
      grayness[point] = u_grayness[i].val;
      // edge_idx[point] = u_edges[i].idx;
      // rho[point]      = u_edges[i].val;
   }

   // write vtk-file

   const double  noVal = (args.vtk == vtk::DATAFORMAT::ASCII) ? -1.0 : nan("1");
   const int64_t noIdx = -1;

   // todo parallelize output over all MPI procs
   if (param.dd.mod.rnk == 0)
   {
      vtk::StructuredWriter vtk_writer(args.outfile,
                                       vtk::DATAFORMAT(args.vtk),
                                       "Optimization result",
                                       param.dd.dof.rnk,
                                       param.dd.dof.np,
                                       param.dd.dof.comm,
                                       param.domain,
                                       {args.d, args.d, args.d},
                                       {0.0, 0.0, 0.0},
                                       planes);
      vtk_writer.add_data_scalar<int>("particle", particle, 0);
      vtk_writer.add_data_scalar<int64_t>("material_idx", mat_idx, noIdx);
      // vtk_writer.add_data_scalar<int64_t>("edge_idx", edge_idx, noIdx);
      vtk_writer.add_data_scalar<double>("grayness", grayness, noVal);
      // vtk_writer.add_data_scalar<double>("rho", rho, noVal);
   }
}

template <int INTERPOLATION>
void compare_with_mono_material(const InputArgs&                        args,
                                Parameters&                             param,
                                std::shared_ptr<SGP_DDA<INTERPOLATION>> alg,
                                const std::vector<sgp::Result>&         u_grayness)
{
   sleep(10);

   auto N = dda_N_local();

   double J;

   if (param.rank == 0)
   {
      std::cout << "\n>>>>>>>>>> COMPARE RESULT TO MONO-MATERIAL CONFIGURATIONS <<<<<<<<<<\n\n";
   }

   for (size_t i = 0; i < param.materials.size(); ++i)
   {
      param.U0 = material_configuration(param.coating_layer, i, args.u_core);

      J = alg->computeJ(param.U0);

      if (param.rank == 0)
      {
         std::cout << ">>> J(u_coating = u_" << i << ") = " << J << std::endl;
      }
   }

   J = alg->getJ();

   if (param.rank == 0)
   {
      std::cout << ">>> J(u*) = " << J << std::endl;
   }

   /* get J(u**) where u** is the solution u* shiftet towards the
        closest admissible configuration.
   */
   std::vector<size_t> u(N);

   for (size_t i = 0; i < N; ++i)
   {
      u[i] = u_grayness[i].idx;
   }

   J = alg->computeJ(u);

   if (param.rank == 0)
   {
      std::cout << ">>> J(u*_admissible) = " << J << std::endl;
   }
}

template <int INTERPOLATION>
void run_optimization(const InputArgs& args, Parameters& param)
{
   ////////////////////////////////////////////////////////
   // init SGP
   ////////////////////////////////////////////////////////

   // setup initial configuration
   compute_coating_layer(args, param);
   param.U0 = (args.u0file.empty()) ? material_configuration(param.coating_layer, args.u0, args.u_core) :
                                      read_backup(args.u0file, param.dd.dof.rnk);

   auto alg = initialize_optimization<INTERPOLATION>(args, param);

   ////////////////////////////////////////////////////////
   // run optimization
   ////////////////////////////////////////////////////////

   sleep(10);

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> RUN OPTIMIZATION <<<<<<<<<<\n\n";
   }

   auto u_grayness = alg->solve(args.tau, args.nu_inner, args.nu_outer, args.omega, param.U0);

   ////////////////////////////////////////////////////////
   // write backup file
   ////////////////////////////////////////////////////////

   if (not args.backupfile.empty())
   {
      if (args.verbose && (param.rank == 0))
      {
         std::cout << "\n>>>>>>>>>> WRITE CONFIGURATION TO BACKUP FILE <<<<<<<<<<\n\n";
      }
      write_backup(u_grayness, args.backupfile, param.dd.dof.rnk);
   }

   ////////////////////////////////////////////////////////
   // write extinction values for each wavelength and incident field
   ////////////////////////////////////////////////////////

   show_results_properties(args, param, alg);

   ////////////////////////////////////////////////////////
   // write iter info
   ////////////////////////////////////////////////////////

   write_iter_info(args, param, alg);

   ////////////////////////////////////////////////////////
   // write vtk output
   ////////////////////////////////////////////////////////

   if (args.vtk != vtk::DATAFORMAT::NONE)
   {
      vtk_output(args, param, u_grayness);
   }

   ////////////////////////////////////////////////////////
   // validation
   ////////////////////////////////////////////////////////

   if (args.compare and args.verbose)
   {
      compare_with_mono_material(args, param, alg, u_grayness);
   }
}

int main(int argc, char** argv)
{
   MPI_Init(&argc, &argv);
   PetscInitialize(&argc, &argv, nullptr, nullptr);

   Parameters param;
   MPI_Comm_rank(MPI_COMM_WORLD, &param.rank);
   MPI_Comm_size(MPI_COMM_WORLD, &param.np_world);

   if (argc != 2)
   {
      if (param.rank == 0)
         std::cout << "Usage: " << argv[0] << " [PARAMETERFILE]\n";

      PetscFinalize();
      MPI_Finalize();
      exit(1);
   }

   ////////////////////////////////////////////////////////
   // read parameters
   ////////////////////////////////////////////////////////

   auto args = read_arguments(param, argv[1]);

   read_data(args, param);

   ////////////////////////////////////////////////////////
   // Domain Decomposition
   ////////////////////////////////////////////////////////

   domain_decomposition(args, param);

   ////////////////////////////////////////////////////////
   // init DDA
   ////////////////////////////////////////////////////////

   initialize_dda(args, param);

   ////////////////////////////////////////////////////////
   // optimization
   ////////////////////////////////////////////////////////

   switch (args.interpolation)
   {
      case 1:
         run_optimization<1>(args, param);
         break;
         //todo
         // case 2:
         //    run_optimization<2>(args, param);
         //    break;
         // case 3:
         //    run_optimization<3>(args, param);
         //    break;

      default:
         throw std::invalid_argument(
             // "Only linear, quadratic and cubic interpolation are implemented -> n_en must be in {2,3,4}.");
             "Only linear interpolation is implemented -> n_en = 2.");
         break;
   }

   ////////////////////////////////////////////////////////
   // finalize
   ////////////////////////////////////////////////////////

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> PROGRAM FINISHED <<<<<<<<<<\n";
   }

   dda_free();
   PetscFinalize();
   MPI_Finalize();
   return 0;
}
