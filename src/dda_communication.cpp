/*
    Conversion between complex numbers from STL to OpenDDA and vice versa.

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#include "dda_communication.hpp"

/*  convert material parameters from complex_t (n,k) to dcomplex epsilon (used within OpenDDA)
    @param src  vector of [refractive index / extinction coefficient] - pairs
    @param dst  vector of permettivities
*/
void convert_material(const C_vec& src, C_vec_dda& dst)
{
    for (int i = 0; i < src.size(); ++i)
    {
        // epsilon = (n^2 - k^2) + i*(2*n*k)
        dst[i].dat[0] = src[i].real()*src[i].real() - src[i].imag()*src[i].imag();
        dst[i].dat[1] = 2*src[i].real()*src[i].imag();
    }
}


/*  convert OpenDDA solution vector to C3_vec
    @param  src     pointer to OpenDDA solution
    @param  dst     vector where the solution shall be stored
*/
void convert_solution(const dcomplex* src, C3_vec& dst)
{
    const size_t n = dst.size();
    // ordering of vector components in OpenDDA: [x1,...xn,y1,...,yn,z1,...,zn]
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            dst[i][j].real(src[j * n + i].dat[0]);
            dst[i][j].imag(src[j * n + i].dat[1]);
        }
    }
}

/*  convert C3_vec to OpenDDA solution vector
    @param  src     vector where the solution shall be stored
    @param  dst     pointer to OpenDDA solution
*/
void convert_solution(const C3_vec& src, dcomplex* dst)
{
    const size_t n = src.size();
    // ordering of vector components in OpenDDA: [x1,...xn,y1,...,yn,z1,...,zn]
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            dst[j * n + i].dat[0] = src[i][j].real();
            dst[j * n + i].dat[1] = src[i][j].imag();
        }
    }
}

/* compute factor for Clausius-Mossotti polarisability
    @param nk   complex refractive index n+ik
    @returns alpha_cm
*/
complex_t alpha(const complex_t& nk)
{
    complex_t epsilon = nk*nk;
    return (epsilon - 1.0) / (epsilon + 2.0) * dda_get_density();
}
