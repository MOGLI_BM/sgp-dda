#!/usr/bin/python3

from sys import argv, exit
import os
import re
import csv

SGP = ""


def get_np():
    np = 1
    p = re.compile(r"\s*np_.*=\s*\[\s*(\d+)\s+(\d+)\s*\].*")
    with open("config", "r") as config:
        for line in config:
            m = p.match(line)
            if m:
                n_min = int(m.group(1))
                n_max = int(m.group(2))
                if n_min != 1:
                    exit("ERROR: Illegal argument in config file: np_min must be 1!")
                np *= n_max
    return np


def parse(file, ignore_last_column):
    data = []
    with open(file, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            try:
                if ignore_last_column:
                    data.append([float(d) for d in row[:-1]])
                else:
                    data.append([float(d) for d in row])
            except:
                pass
    return data


def compare_output():
    with open('tol', 'r') as tolF:
        tolerance = [float(line) for line in tolF]
    data = ['ext', 'iter']
    for file,eps in zip(data,tolerance):
        tst = parse('%s.tst'%file, file=='iter')
        ref = parse('%s.ref'%file, file=='iter')
        try:
            for tst_r,ref_r in zip(tst,ref):
                for tst_c,ref_c in zip(tst_r,ref_r):
                    if abs(tst_c - ref_c) > eps:
                        print('    error in %s.tst: %f != %f'%(file, tst_c, ref_c))
                        return 1
        except:
            print('    * error in %s.tst: file mismatch!'%(file))
            return 1
    return 0


def run_test(idx: int):
    os.chdir("test_%d" % idx)
    failed = 0

    for np in [1, get_np()]:
        info = '%d: '%(idx)
        if (np == 1):
            info += '(serial)'
        else:
            info += '(mpi)'

        print('  %s ...'%info)
        os.system("mpirun -np %d %s config" % (np, SGP))

        if compare_output():
            print('    FAILED!')
            failed += 1
        else:
            print('    PASSED!')

    os.chdir("..")
    return failed



if __name__ == "__main__":

    if len(argv) < 2:
        exit("Usage: ./runtest PATH_TO_BUILD_DIR [TESTIDX]")

    SGP = "../%s/bin/DDA_Opt" % (argv[1])

    tests = []
    if len(argv) > 2:
        tests.append(int(argv[2]))

    TESTDIR = re.compile(r'.*_(\d+)')

    if len(tests) == 0:
        for p,dirs,f in os.walk('.'):
            for tname in dirs:
                m = TESTDIR.match(tname)
                if m:
                    tests.append(int(m.group(1)))

    print(">>> running %d+%d tests"%(len(tests), len(tests)))

    failed = 0
    for idx in tests:
        failed += run_test(idx)

    if failed:
        print(">>> %d out of %d test failed!"%(failed, 2*len(tests)))
        exit(1)
    else:
        print(">>> all tests successfull!")
        exit(0)
