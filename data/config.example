#####################################################################
### GEOMETRY PARAMETERS ###
#####################################################################

# d:        Spacing between dipoles
# N:        maximum number of dipoles in each direction [N_x N_y N_z]
# shape:    admissible values for the shape are
#            ellipsoid
#            cuboid
#            FILENAME
#               where FILENAME must be a conforming file, i.e.
#               each line describes one dipole by its multi-index:
#               x,y,z with 0 <= x < N_x
# coating:  width of coating layer (material optimization will only
#                be performed in coating layer)
#           To optimize entire particle, set coating = 0!
# u_core:   node-index for the core material. If coating = 0, this
#           parameter will be ignored.

#d               = 0.05
# N               = [10 10 10]
# shape           = ellipsoid
d = 0.014286
N = [26 35 35]
shape           = ../data/custom_shape.example
coating         = 0
u_core          = 0

#####################################################################
### PARALLELIZATION ###
#####################################################################

# User defined settings for distribution of MPI processes among
# various directions of parallelism:
# np_dof: number of procs used to parallelize over the dipoles
#           Note:  In our experiments, choosing smaller values
#             for the upper limit of np_dof yielded better performance
# np_wav: number of procs used to parallelize over wavelengths
#                wavelengths will be used
# np_E:   number of procs for parallelization over incident parameters
#
# For each of these 3 directions, the minimum and maximum number
# of processes must be specified, e.g., np = [min max].
#
# Note: Unless min=max for at least 2 directions, the actual
#   distribution of MPI-ranks will be determined automatically.
#   The algorithm will always try to maximize np_dof in order to keep
#   the memory footprint low.
#   Furhermore, np_wav will be chosen as small as possible, since
#   different runtimes are to be expected for different material parameters.
#
# If no valid combination (i.e. np_dof * np_wav * np_E = np)
# is found, the program will abort!

np_dof          = [1 32]
np_wav          = [1 2]
np_E            = [1 2]

#####################################################################
### DDA PARAMETERS ###
#####################################################################

# Scattering directions in spherical coordinates [phi theta]
# phi:      clockwise angle [0,360], in the [x_{L}y_{L}]-plane,
#           between the scattering plane and the +x_{L}-axis looking
#           in the +z_{L}-axis direction, i.e., the angle between
#           the scattering plane and the xz-plane. The scattering
#           plane is the plane containing the incident direction and
#           the scattering direction [+z_{L}-axis & n_{sca}],
#           phi=0 => scattering plane=xz-plane,
#           phi=90 => scattering plane=yz-plane
# theta:    polar/zenith angle [0,180], in the scattering plane,
#           between the incident direction +z_{L}-axis and the
#           scattering direction n_{sca}.
#           theta=0 => forward scattering, scattering dir=+z_{L}
#           theta=180 => backward scattering, scattering dir=-z_{L}

scattering      = [0 0]

#####################################################################

# Linear solver used for each DDA problem
# 0:    CG
# 1:    CG Squared
# 2:    BiCG, BiConjugate-Gradients
# 3:    BiCG for symmetric systems
# 4:    BiCGSTAB, i.e., Stabilized BiCG
# 5:    BiCGSTAB variant1 based on multiple Lanczos starting vectors
# 6:    BiCGSTAB variant2 based on multiple Lanczos starting vectors
# 7:    BiCGSTAB variant3 based on multiple Lanczos starting vectors
# 8:    BiCGSTAB variant4
# 9:    QMR, Quasi-minimal residual with coupled two-term recurrences
# 10:   QMR for symmetric systems
# 11:   Transpose-free QMR
#
# preconditioner: 0 -> none
#                 1 -> PointJacobi
# initial_guess_P_old: use solution P from previous sgp iteration as
#                   initial guess for the linear solver for the
#                   dda-problem. (otherwise use P=0)
# dda_tol       : tolerance for linear solver
# dda_max_iter  : maximum number of iterations before aborting
# starting_vec  : number of starting vectors for 5--8 (must be > 0)

solver_type     = 10
preconditioner  = 0
initial_guess_P_old = 0
dda_tol         = 1e-8
dda_max_iter    = 10000
starting_vec    = 3

#####################################################################
### OPTIMIZATION (SGP) PARAMETERS ###
#####################################################################

# if (verbose!=0): print progress information to stdout
#
# if (compare!=0): compare optimization result to mono-material
#                      configurations
#
# n_en: Number of nodes describing an edge in the material graph
#   Also defines interpolation scheme used for evaluating
#   v_i(rho) along the edges, i.e. n_en=2 => linear interpolation,
#   n_en=3 => quadratic interpolation, etc.
#
#   Currently only n_en=2 is supported!
#
# output_iterations: path to output file for general information
#                   if present, the information on each iteration
#                   will be written into this file
#
# output_extinction: path to output file for extinction per
#                   wavelength of final material configuration.

compare         = 1
verbose         = 1
n_en            = 2
output_iterations = ../data/iter_table.csv
output_extinction = ../data/ext_table.csv

#####################################################################

# adjust filter term:
#
# filter_value: <=1  -> no filtering
#               >1  -> add filter term with filter matrix F to
#                      separable model where
#                           F_ij = max{filter_value - ||d_i - d_j||^2, 0}
#                      with d_i being the index-vector of the i-th dipole,
#                      i.e., ||d_i-d_j|| is the normalized distance.
#
# filter_penalty: factor for filtering term in separable model
#
# load_F: path to file from which filter matrix shall be loaded.
#          Note: If load_F is set, filter_value will be ignored.
#
# store_F: path to file where filter matrix shall be stored.

filter_value    = 0
filter_penalty  = 0
# load_F          = ../data/F.mat
# store_F          = ../data/F.mat

#####################################################################

# output
#
# vtk_datatype: 0 = no vtk-output
#               1 = BINARY # not implemented yet
#               2 = ASCII
#
# result:       Path to vtk file where the result u* shall be written
# backup:       Path to backup-file which can be used to restart
#                the algorithm with intermediate result
# startwithBU:  Path to backup file to start from.
#               Note: This will only work when restarting with
#                   the same problem domain and size and same number
#                   of MPI procs.
#                   Furthermore, u0 and u_core will be overriden by
#                   intermediate values from backup file.

vtk_datatype    = 2
result          = ../data/u.vti
backup          = ../data/backup.
# startwithBU     = ../data/previousbackup.

#####################################################################

# Path to file containing the incident field parameters
# Each line contains a incident field description by means of euler
# rotation of the target frame and x,y components of the normalized
# Jones vector E0, e.g.
#       phi theta psi (Re(E0_x), Im(E0_x)) (Re(E0_y), Im(E0_y))
#
# Notes (Jones vector): * defined looking in the direction of
#                           propagation, i.e., +z
#                       * Normalization is performed automatically
#
# Notes (euler rotation)
# phi:      1st Euler angle phi: Azimuthal angle [0,360]. The
#           clockwise angle in the [x_{L}y_{L}]-plane about the
#           z_{L}-axis looking in the +z_{L} direction.
# theta:    2nd Euler angle theta: Polar/zenith angle [0,180].
#           The clockwise angle in the [x^{phi}z^{phi}]-plane about
#           the y^{phi}-axis looking in the +y^{phi}-axis direction.
# psi:      3rd Euler angle psi: Azimuthal angle [0,360]. The clock-
#           wise angle in the [x^{theta}y^{theta}]-plane about the
#           z^{theta}-axis looking in the +z^{theta}-axis direction.

incident_params = ../data/incident_field.example

#####################################################################

# Path to file containing the list of admissible materials
# and wavelengths.
# The first line contains a list of wavelengths, the other lines
# contain the associated material parameters, i.e., the file
# comprises n_materials+1 lines.
# The i-th line contains the parameters n (refractive index) and
# k (extinction coefficient) of the (i-2)-th node in the material
# graph, given by (n,k) for i = 2,...,n_materials-1.
# The j-th column corresponds to the j-th wavelength.

material_bib    = ../data/materials.example

#####################################################################

# Path to file describing the edges of the material graph.
# Each line in material_graph must contains an edge in the
# graph of admissible materials given by k node-indices
# n_0 n_1 ... n_{n_en-1}, s.th. n_k < n_materials.

 material_graph  = ../data/edges.example
# material_graph  = ../data/edges_quadratic.example

#####################################################################

# Stopping criteria for the optimization:
# criterion:        Type of stopping criterion where
#                   criterion=0 => |j_new - j_old|
#                   criterion=1 => ||u_new-u_old||
# tol:              Outer loop runs until criterion < tol
# max_iter:         Maximum number of iterations before the
#                   algorithm is considered to be diverging.
# max_iter_inner:   Maximum number of iterations for the inner
#                   loop before result is accepted.

criterion       = 0
tol             = 1e-8
max_iter        = 10
max_iter_inner  = 10

#####################################################################

# sgp-parameters:
# tau:      reciprocal step size
# nu_inner: Factor to increase tau in the inner loop; > 1
# nu_outer: Factor to decrease tau in outer loop; < 1
#           s.th. (nu_inner*nu_outer) < 1 !
# omega:    Scaling factor for penalty J_pen. Use omega=0 unless
#           this yields inadmissible materials in the solution.
#           note: omega=4 => omega*J_pen = grayness
# u0:       node-index for starting configuration

tau             = 1e-8
nu_inner        = 10
nu_outer        = 0.0833333
omega           = 0
u0              = 2

#####################################################################

# Multi-section parameters:
# n_sec:    Number of nodes for edge multi-section, including
#           the boundary nodes. Must be greater than 3!
# eps_rho:  Multi-section converged if |rho_r - rho_l| < eps_rho
# eps_S:    Multi-section converged if
#           max_{rho in [rho_l, rho_r]} S[u](v_i(rho))
#           - min_{rho in [rho_l, rho_r]} S[u](v_i(rho)) < eps_S

n_sec           = 10
eps_rho         = 1e-10
eps_S           = 1e-10

#####################################################################
