/*
    Specialized Class representing an SPG-algorithm using discrete dipole approximation (DDA)

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include "dda_communication.hpp"
#include "petsc_matrix.hpp"
#include "petsc_vector.hpp"
#include "sgp.hpp"

/* Specialized Class representing an SPG-algorithm using discrete dipole approximation (DDA)
       @tparam INTERPOLATION   type of interpolation scheme for edges of material graph, e.g. INTERPOLATION=1 for linear interpolation
*/
template <int INTERPOLATION>
class SGP_DDA : public sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>
{
 public:
   // members from parent
   using edge_t = typename sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>::edge_t;

   using sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>::N;
   using sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>::n_mod1;
   using sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>::n_mod2;
   using sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>::u_old;

   /* Ctor
        @param  N_          number of (local) degrees of freedom
        @param  materials_  vector of admissible materials, i.e., the nodes in the material-graph
                            Note: each material is given by a vector of material properties where the k-th
                                entry corresponds to the k-th load parameter of first kind, i.e., mods_1_[k]
        @param  edges_      edges of graph of admissible materials; an edge is defined by INTERPOLATION+1 node-indices.
        @param  mods_1_     vector of k1 load parameters of first kind, i.e., load parameters, which also change the material parameters
        @param  mods_2_     vector of k2 load parameters of second kind, i.e., load parameters, which don't change material parameters
        @param  dd_         Domain decomposition information
        @param  F_          Filter Matrix
        @param  filter_penalty penalty factor for filter term in separable model
        @param  verbose_    if true, state information will be printed to std::out in each iteration
        @param  initial_guess_P_old if true, P_old will be used as initial guess for the linear solver in eval()/eval_adj()
    */
   SGP_DDA(const size_t                             N_,
           const sgp::Arr2D<complex_t>&             materials_,
           const sgp::Arr1D<edge_t>&                edges_,
           const sgp::Arr1D<wave_t>&                mods_1_,
           const sgp::Arr1D<polarization_t>&        mods_2_,
           const sgp::mpi::DomainDecomposition      dd_,
           const std::shared_ptr<const PETScMatrix> F_                  = nullptr,
           const double                             filter_penalty      = 0,
           const bool                               verbose_            = false,
           const bool                               initial_guess_P_old = false)
   : SGP_DDA(sgp::Arr1D<bool>(N_, true),
             materials_,
             edges_,
             mods_1_,
             mods_2_,
             dd_,
             F_,
             filter_penalty,
             verbose_,
             initial_guess_P_old)
   {}

   /* Ctor
        @param  to_optimize vector of flags s.th. to_optimize[k] specifies whether the k-th node shall be
                            optimized or not (i.e. it keeps its initial value)
        @param  materials_  vector of admissible materials, i.e., the nodes in the material-graph
                            Note: each material is given by a vector of material properties where the k-th
                                entry corresponds to the k-th load parameter of first kind, i.e., mods_1_[k]
        @param  edges_      edges of graph of admissible materials; an edge is defined by INTERPOLATION+1 node-indices.
        @param  mods_1_     vector of k1 load parameters of first kind, i.e., load parameters, which also change the material parameters
        @param  mods_2_     vector of k2 load parameters of second kind, i.e., load parameters, which don't change material parameters
        @param  dd_         Domain decomposition information
        @param  F_          Filter Matrix
        @param  filter_penalty penalty factor for filter term in separable model
        @param  verbose_    if true, state information will be printed to std::out in each iteration
        @param  initial_guess_P_old if true, P_old will be used as initial guess for the linear solver in eval()/eval_adj()
    */
   SGP_DDA(const sgp::Arr1D<bool>                   to_optimize,
           const sgp::Arr2D<complex_t>&             materials_,
           const sgp::Arr1D<edge_t>&                edges_,
           const sgp::Arr1D<wave_t>&                mods_1_,
           const sgp::Arr1D<polarization_t>&        mods_2_,
           const sgp::mpi::DomainDecomposition      dd_,
           const std::shared_ptr<const PETScMatrix> F_                  = nullptr,
           const double                             filter_penalty      = 0,
           const bool                               verbose_            = false,
           const bool                               initial_guess_P_old = false);

   void prepare_iteration();

   /* solve A(u)P(u) = B -- to be implemented in derived class.
        @param  u       material configuration corresponding to wavelength m1
        @param  P       reference to solution vector
        @param  wavelen wavelength
        @param  pol     incident polarization, given by (frame_rotation, polarization)
        @return         J(u), i.e., energy functional evaluated for current configuration
    */
   double eval(const sgp::Arr1D<complex_t>& u, sgp::Arr1D<complex_3d>& P, const wave_t& wavelen, const polarization_t& pol) const;

   /* solve adjoint problem corresponding to A(u)P(u) = B -- to be implemented in derived class.
        @param  u       material configuration corresponding to wavelength m1
        @param  Q       reference to solution vector of adjoint problem
        @param  wavelen wavelength
        @param  pol     incident polarization, given by (frame_rotation, polarization)
    */
   void eval_adj(const sgp::Arr1D<complex_t>& u,
                 sgp::Arr1D<complex_3d>&      Q,
                 const wave_t&                wavelen,
                 const polarization_t&        pol) const;

   /* compute i-th term (i=0,...,N-1) of problem specific separable approximation S -- to be implemented in derived class.
        @param  u_i     i-th component of current material-vector
        @param  v_i     i-th component of test-vector
        @param  P_i     i-th component of solution
        @param  Q_i     i-th component of solution of adjoint problem
        @param  J_pen   penalty for inter-material configuration corresponding to v_i
        @param  wavelen wavelength
        @param  pol     incident polarization given by pol = (frame_rotation, polarization)
        @param  tau     penalty factor for |u-v|^2
        @param  i       local index in {0,...,N-1}
        @param  k1      global index of mod1 in {0,...,n_mod1 - 1}
    */
   double S(const complex_t&      u_i,
            const complex_t&      v_i,
            const complex_3d&     P_i,
            const complex_3d&     Q_i,
            const double          J_pen_v_i,
            const wave_t&         wavelen,
            const polarization_t& pol,
            const double          tau,
            const int             i,
            const int             k1,
            double&               j_reg) const;

   /* compute the i-th term of the scalar product u*u -- to be implemented in derived class.
        @ param u_i     i-th component of u
        @ return i-th term of u*u
    */
   double norm2(const complex_t& u_i) const;

   /* rule for computing total energy J(u) based on the partial results J(u_mod)
        for each load parameter.
        @param J    matrix of partial results s.th. J[j][k] is associated with waves[j] and pols[k]
        @param waves vector of wavelengths
        @param pols  vector of incident polarizations
        @return     (weighted) sum of the components J_k
    */
   double sumJ(const sgp::Arr2D<double>& J, const sgp::Arr1D<wave_t>& waves, const sgp::Arr1D<polarization_t>& pols) const;

   /* rule for computing the gradient of sumJ, required for summing up S[u](vi) -- to be implemented in derived class
        @param J    matrix of partial results s.th. J[j][k] is associated with waves[j] and pols[k]
        @param dJ   matrix of partial results for dJ/du_i, s.th. dJ[j][k] is associated with waves[j] and pols[k]
        @param waves vector of wavelengths
        @param pols  vector of incident polarizations
        @return     (weighted) sum of the components s_k
    */
   double gradSumJ(const sgp::Arr2D<double>&         J,
                   const sgp::Arr2D<double>&         dJ,
                   const sgp::Arr1D<wave_t>&         waves,
                   const sgp::Arr1D<polarization_t>& pols) const;

   size_t local_memory_usage()
   {
      size_t total = sgp::Algorithm<complex_t, complex_3d, wave_t, polarization_t, INTERPOLATION>::local_memory_usage();
      total += _u_cpy.size() * sizeof(dcomplex);
      return total;
   }

 private:
   void eval_generic(const sgp::Arr1D<complex_t>& u,
                     sgp::Arr1D<complex_3d>&      P,
                     const real_t&                wavelen,
                     const polarization_t&        pol,
                     const bool                   adjoint) const;

   mutable C_vec_dda                        _u_cpy;   // copy of u_old for communication with OpenDDA;
   std::shared_ptr<PETScVector>             _u_petsc; // copy of u_old for communication with PETSc;
   std::shared_ptr<PETScVector>             _diagM;   // diag(M) with M = (F-I)^T(F-I)
   std::shared_ptr<PETScMatrix>             _MmD;     // M - diag(M) with M = (F-I)^T(F-I)
   sgp::Arr1D<std::shared_ptr<PETScVector>> _R;       // [M - diag(M)]*u_old
   const bool                               _initial_guess_P_old;
   double                                   _filt_pen; // penalty factor for filter term
};
