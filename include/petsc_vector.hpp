/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <mpi.h>
#include <petsc.h>

#include "dda_communication.hpp"
#include "sgp.hpp"

/// Wrapper class for PETSc vector usage
class PETScVector
{
 private:
   PETScVector()
   : assembled_(true)
   {}

 public:
   PETScVector(const PETScVector&) = delete;
   PETScVector(PETScVector&&)      = delete;

   PETScVector(int localSize, int globalSize, const MPI_Comm& comm = MPI_COMM_WORLD)
   : n_(static_cast<PetscInt>(localSize))
   , N_(static_cast<PetscInt>(globalSize))
   , comm_(comm)
   , assembled_(false)
   , has_bound_data_(false)
   {
      VecCreate(comm, &vec_);
      VecSetSizes(vec_, n_, N_);
      VecSetFromOptions(vec_);
      VecSet(vec_, 0.0);

      VecGetOwnershipRange(vec_, &idx0_, NULL);
   }

   PETScVector(sgp::Arr1D<complex_t>& vec, int globalSize, const MPI_Comm& comm = MPI_COMM_WORLD)
   : PETScVector(vec.size(), globalSize, comm)
   {
      bind(vec);
   }

   ~PETScVector()
   {
      reset();
      VecDestroy(&vec_);
   }

   inline void bind(sgp::Arr1D<complex_t>& vec)
   {
      reset();
      VecPlaceArray(vec_, vec.data());
      has_bound_data_ = true;
   }

   inline void print(const std::string& filename = "") const
   {
      assemble();

      if (filename.empty())
      {
         VecView(vec_, PETSC_VIEWER_STDOUT_(comm_));
      }
      else
      {
         PetscViewer viewer;
         PetscViewerASCIIOpen(comm_, filename.c_str(), &viewer);
         VecView(vec_, viewer);
         PetscViewerDestroy(&viewer);
      }
   }

   // set value at row
   inline void setValue(int row, complex_t value)
   {
      PetscComplex petscVal = static_cast<PetscComplex>(value);
      VecSetValue(vec_, static_cast<PetscInt>(row), petscVal, INSERT_VALUES);
   }

   inline complex_t getValue(int row) const
   {
      assemble();
      PetscComplex x;
      PetscInt     i = static_cast<PetscInt>(row);
      VecGetValues(vec_, 1, &i, &x);
      return x;
   }

   /* gather vector info from all processes into new PETScVector */
   std::shared_ptr<PETScVector> gather()
   {
      VecScatter                   ctx;
      std::shared_ptr<PETScVector> vec(new PETScVector());
      VecScatterCreateToAll(vec_, &ctx, &(vec->vec_));
      VecScatterBegin(ctx, vec_, vec->vec_, INSERT_VALUES, SCATTER_FORWARD);
      VecScatterEnd(ctx, vec_, vec->vec_, INSERT_VALUES, SCATTER_FORWARD);
      VecScatterDestroy(&ctx);
      return vec;
   }

   /* get pointer to underlying data array
      if data is no longer needed, return_data() must be called!
      !USE WITH CAUTION!
   */
   inline complex_t* get_data()
   {
      PetscComplex* data;
      VecGetArray(vec_, &data);
      return data;
   }

   /* return pointer to underlying data array obtained from get_data();
   */
   inline void return_data(complex_t** data) { VecRestoreArray(vec_, data); }

   // // set value at the row corresponding to local_idx
   inline void setValue_loc(int local_idx, complex_t value) { setValue(idx0_ + local_idx, value); }

   inline int idx0() const { return int(idx0_); }

   // // get value at the row corresponding to local_idx
   inline complex_t getValue_loc(int local_idx) const { return getValue(idx0_ + local_idx); }

   friend class PETScMatrix;

 private:
   inline void reset()
   {
      if (has_bound_data_)
         VecResetArray(vec_);
   }

   /* assemble vector -- must be called when assembly is done */
   inline void assemble() const
   {
      if (not assembled_)
      {
         VecAssemblyBegin(vec_);
         VecAssemblyEnd(vec_);
         assembled_ = true;
      }
   }

   PetscInt     idx0_;
   PetscInt     n_;
   PetscInt     N_;
   MPI_Comm     comm_;
   mutable bool assembled_;
   Vec          vec_;
   bool         has_bound_data_;
};
