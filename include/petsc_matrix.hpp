/*
   Copyright (c) 2017-2021 Nils Kohl, Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <mpi.h>
#include <petsc.h>

#include "dda_communication.hpp"
#include "petsc_vector.hpp"
#include "sgp.hpp"

/// Wrapper class for PETSc sparse matrix usage
class PETScMatrix
{
 private:
   // construct empyt matrix container //
   PETScMatrix(int localSize, int globalSize, const MPI_Comm& comm)
   : comm_(comm)
   , assembled_(false)
   , n_(localSize)
   , N_(globalSize)
   {}

 public:
   PETScMatrix(const PETScMatrix& A)
   : PETScMatrix(A.n_, A.N_, A.comm_)
   {
      MatDuplicate(A.mat_, MAT_COPY_VALUES, &mat_);
   }

   PETScMatrix(PETScMatrix&& A)
   : PETScMatrix(A.n_, A.N_, A.comm_)
   {
      MatDuplicate(A.mat_, MAT_COPY_VALUES, &mat_);
   }

   PETScMatrix(int localSize, int globalSize, int nonzeros, const std::string& name, const MPI_Comm& comm)
   : PETScMatrix(localSize, globalSize, comm)
   {
      MatCreate(comm, &mat_);
      MatSetType(mat_, MATMPIAIJ);
      MatSetSizes(mat_, (PetscInt) localSize, (PetscInt) localSize, (PetscInt) globalSize, (PetscInt) globalSize);
      MatMPIAIJSetPreallocation(mat_, (PetscInt) nonzeros, NULL, (PetscInt) nonzeros, NULL);
      PetscObjectSetName((PetscObject) mat_, name.c_str());
   }

   PETScMatrix(const MPI_Comm& comm, const std::string& filename)
   : comm_(comm)
   , assembled_(false)
   {
      // create matrix
      MatCreate(comm, &mat_);
      MatSetType(mat_, MATMPIAIJ);

      // load from file
      PetscViewer viewer;
      PetscViewerBinaryOpen(comm_, filename.c_str(), FILE_MODE_READ, &viewer);
      MatLoad(mat_, viewer);
      PetscViewerDestroy(&viewer);

      // extract dimensions
      MatGetLocalSize(mat_, &n_, NULL);
      MatGetSize(mat_, &N_, NULL);
   }

   ~PETScMatrix() {MatDestroy(&mat_); }

   inline void print(const std::string& filename = "", bool binary = false) const
   {
      assemble(true);

      if (filename.empty())
      {
         MatView(mat_, PETSC_VIEWER_STDOUT_(comm_));
      }
      else
      {
         PetscViewer viewer;
         if (binary)
         {
            PetscViewerBinaryOpen(comm_, filename.c_str(), FILE_MODE_WRITE, &viewer);
         }
         else
         {
            PetscViewerASCIIOpen(comm_, filename.c_str(), &viewer);
         }

         MatView(mat_, viewer);
         PetscViewerDestroy(&viewer);
      }
   }

   inline void addValue(int row, int col, real_t value)
   {
      PetscReal petscVal = static_cast<PetscReal>(value);
      MatSetValue(mat_, static_cast<PetscInt>(row), static_cast<PetscInt>(col), petscVal, ADD_VALUES);
   }

   inline void setValue(int row, int col, real_t value)
   {
      PetscReal petscVal = static_cast<PetscReal>(value);
      MatSetValue(mat_, static_cast<PetscInt>(row), static_cast<PetscInt>(col), petscVal, INSERT_VALUES);
   }

   inline real_t getValue(int row, int col) const
   {
      assemble();
      PetscComplex a_ij;
      PetscInt     i = static_cast<PetscInt>(row);
      PetscInt     j = static_cast<PetscInt>(col);
      MatGetValues(mat_, 1, &i, 1, &j, &a_ij);
      return complex_t(a_ij).real();
   }

   /* compute y = A*x */
   // inline void apply(const sgp::Arr1D<complex_t>& x, sgp::Arr1D<complex_t>& y)
   inline void apply(const PETScVector& x, PETScVector& y) const
   {
      assemble(true);
      x.assemble();
      MatMult(mat_, x.vec_, y.vec_);
   }

   /* compute the symmetric product A^t * A */
   std::shared_ptr<PETScMatrix> AtA() const
   {
      assemble(true);
      std::shared_ptr<PETScMatrix> C(new PETScMatrix(n_, N_, comm_));
      MatTransposeMatMult(mat_, mat_, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &(C->mat_));
      return C;
   }

   /* compute C = A+D, with diagonal matrix D represented as vector */
   std::shared_ptr<PETScMatrix> addDiag(const PETScVector& diag) const
   {
      assemble(true);
      auto C = std::make_shared<PETScMatrix>(*this);
      MatDiagonalSet(C->mat_, diag.vec_, ADD_VALUES);
      return C;
   }

   /* compute C = A-diag(A)+D, with diagonal matrix D represented as vector */
   std::shared_ptr<PETScMatrix> setDiag(const PETScVector& diag) const
   {
      assemble(true);
      auto C = std::make_shared<PETScMatrix>(*this);
      MatDiagonalSet(C->mat_, diag.vec_, INSERT_VALUES);
      return C;
   }

   /* compute C = A+aI */
   std::shared_ptr<PETScMatrix> shiftDiag(const real_t& a) const
   {
      assemble(true);
      auto C = std::make_shared<PETScMatrix>(*this);
      MatShift(C->mat_, (PetscScalar) a);
      return C;
   }

   /* compute C = D*A */
   std::shared_ptr<PETScMatrix> scaleDiag(const PETScVector& diag) const
   {
      assemble(true);
      auto C = std::make_shared<PETScMatrix>(*this);
      MatDiagonalScale(C->mat_, diag.vec_, NULL);
      return C;
   }

   /* get diagonal of the matrix */
   std::shared_ptr<PETScVector> diag() const
   {
      auto v = std::make_shared<PETScVector>(n_, N_, comm_);
      MatGetDiagonal(mat_, v->vec_);
      return v;
   }

 private:
   /* assemble matrix -- must be called when assembly is done */
   inline void assemble(bool final = false) const
   {
      if (final)
      {
         MatAssemblyBegin(mat_, MAT_FINAL_ASSEMBLY);
         MatAssemblyEnd(mat_, MAT_FINAL_ASSEMBLY);
         assembled_ = true;
      }
      else if (not assembled_)
      {
         MatAssemblyBegin(mat_, MAT_FLUSH_ASSEMBLY);
         MatAssemblyEnd(mat_, MAT_FLUSH_ASSEMBLY);
         assembled_ = true;
      }
   }

   MPI_Comm     comm_;
   mutable bool assembled_;
   PetscInt     n_;
   PetscInt     N_;
   Mat          mat_;
};
