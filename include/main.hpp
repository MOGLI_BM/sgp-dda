/*
    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */
#pragma once

#include <chrono>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mpi.h>
#include <thread>

#include "config.hpp"
#include "data.hpp"
#include "sgp_dda.hpp"
#include "vtk_writer.hpp"

enum SHAPE
{
   ELLIPSOID,
   CUBOID,
   OTHER
};

struct InputArgs
{
   bool verbose;
   bool compare;

   std::string outfile_iter;
   std::string outfile_ext;

   std::array<int, 2> np_dof;
   std::array<int, 2> np_wav;
   std::array<int, 2> np_E;

   std::string           shape;
   double                d;
   double                coating;
   std::array<double, 2> scattering;
   std::array<int, 3>    N_xyz;

   int    solver_idx;
   bool   precond;
   bool   initGuessPold;
   double dda_tol;
   int    dda_iter;
   int    starting_vec;

   size_t interpolation;

   double tau;
   double nu_inner;
   double nu_outer;
   double omega;
   size_t u0;
   size_t u_core;

   sgp::StoppingCriterion stopping_crit;

   double tol;
   size_t n_sec;
   double eps_rho;
   double eps_s;
   size_t n_iter_outer;
   size_t n_iter_inner;

   std::string matfile;
   std::string edgefile;
   std::string incidentfile;
   std::string backupfile;
   std::string u0file;

   double      filter_val;
   double      filter_pen;
   std::string store_F;
   std::string load_F;

   int         vtk;
   std::string outfile;
};

struct Parameters
{
   int rank;
   int np_world;

   std::vector<C_vec>               materials;
   std::vector<std::vector<size_t>> edges;
   std::vector<polarization_t>      E;
   std::vector<wave_t>              waves;

   sgp::mpi::DomainDecomposition dd;
   vtk::Point3D<size_t>          domain;
   std::vector<bool>             coating_layer;
   std::shared_ptr<PETScMatrix>  F;

   std::vector<size_t> U0;
};

void sleep(int millsec)
{
   using namespace std::chrono_literals;
   std::this_thread::sleep_for(millsec * 1ms);
}

InputArgs read_arguments(const Parameters& param, const std::string& paramfile)
{
   tyfir::Config input(paramfile);

   InputArgs args;

   args.verbose      = input.get<bool>("verbose");
   args.compare      = input.get<bool>("compare");
   args.outfile_iter = input.get<std::string>("output_iterations", "");
   args.outfile_ext  = input.get<std::string>("output_extinction", "");

   args.np_dof = input.get<std::array<int, 2>>("np_dof");
   args.np_wav = input.get<std::array<int, 2>>("np_wav");
   args.np_E   = input.get<std::array<int, 2>>("np_E");

   args.shape      = input.get<std::string>("shape");
   args.d          = input.get<double>("d");
   args.coating    = input.get<double>("coating");
   args.scattering = input.get<std::array<double, 2>>("scattering");
   args.N_xyz      = input.get<std::array<int, 3>>("N");

   args.solver_idx    = input.get<int>("solver_type");
   args.precond       = input.get<bool>("preconditioner");
   args.initGuessPold = input.get<bool>("initial_guess_P_old");
   args.dda_tol       = input.get<double>("dda_tol");
   args.dda_iter      = input.get<int>("dda_max_iter");
   args.starting_vec  = input.get<int>("starting_vec");

   args.interpolation = input.get<size_t>("n_en") - 1;

   args.tau      = input.get<double>("tau");
   args.nu_inner = input.get<double>("nu_inner");
   args.nu_outer = input.get<double>("nu_outer");
   args.omega    = input.get<double>("omega");
   args.u0       = input.get<size_t>("u0");
   args.u_core   = input.get<size_t>("u_core");

   auto crit_id = input.get<size_t>("criterion");

   if (crit_id > 1)
   {
      throw std::invalid_argument("stopping criterion must be either 0 or 1, i.e., convergence in j or u, respectively.");
   }

   args.stopping_crit = sgp::StoppingCriterion(crit_id);

   args.tol          = input.get<double>("tol");
   args.n_sec        = input.get<size_t>("n_sec");
   args.eps_rho      = input.get<double>("eps_rho");
   args.eps_s        = input.get<double>("eps_S");
   args.n_iter_outer = input.get<size_t>("max_iter");
   args.n_iter_inner = input.get<size_t>("max_iter_inner");

   args.matfile      = input.get<std::string>("material_bib");
   args.edgefile     = input.get<std::string>("material_graph");
   args.incidentfile = input.get<std::string>("incident_params");
   args.backupfile   = input.get<std::string>("backup", "");
   args.u0file       = input.get<std::string>("startwithBU", "");

   args.filter_val = input.get<double>("filter_value", 0.0);
   args.filter_pen = input.get<double>("filter_penalty", 0.0);
   args.store_F    = input.get<std::string>("store_F", "");
   args.load_F     = input.get<std::string>("load_F", "");

   args.vtk     = vtk::DATAFORMAT(input.get<int>("vtk_datatype"));
   args.outfile = input.get<std::string>("result", "");

   if (args.outfile.empty() and args.vtk != vtk::DATAFORMAT::NONE)
   {
      throw std::invalid_argument("path for vtk-output missing");
   }
   // todo fix binary output
   if (args.vtk == vtk::DATAFORMAT::BINARY)
   {
      throw std::runtime_error("binary vtk output not implemented!");
   }

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "input parameters:\n" << input << std::endl;
   }

   return args;
}

void read_data(const InputArgs& args, Parameters& param)
{
   param.domain = {size_t(args.N_xyz[0]), size_t(args.N_xyz[1]), size_t(args.N_xyz[2])};

   // read data from input files

   auto                tmpE = tyfir::read_vectors<complex_t>(args.incidentfile);
   std::vector<double> wavelens;
   std::vector<double> weights;

   for (size_t i = 0; i < tmpE.size(); ++i)
   {
      if (tmpE[i].size() != 5)
      {
         throw std::invalid_argument(
             "INPUT ERROR: Each incident field description must contain exactly 5 values [phi, theta, psi, E0x, E0y]!");
      }

      real_3d    rot{tmpE[i][0].real(), tmpE[i][1].real(), tmpE[i][2].real()};
      complex_2d pol{tmpE[i][3], tmpE[i][4]};

      param.E.push_back({rot, pol});
   }

   param.materials = tyfir::read_vectors<complex_t>(args.matfile);
   param.edges     = tyfir::read_vectors<size_t>(args.edgefile);

   for (auto& el : param.materials[0])
   {
      wavelens.push_back(el.real());
   }

   param.materials.erase(param.materials.begin());

   for (auto& el : param.materials[0])
   {
      weights.push_back(el.real());
   }

   param.materials.erase(param.materials.begin());

   // check input data
   if (weights.size() != wavelens.size())
   {
      throw std::invalid_argument("A scaling factor for the energy functional must be specified for each wavelenght!");
   }

   for (auto& mat : param.materials)
   {
      if (mat.size() != wavelens.size())
      {
         throw std::invalid_argument("Each material must have exactly one (n,k)-pair for each wavelenght!");
      }
   }
   for (auto& edge : param.edges)
   {
      if (edge.size() != args.interpolation + 1)
      {
         throw std::invalid_argument("Each edge is defined by exactly " + std::to_string(args.interpolation + 1) + " nodes!");
      }

      for (auto& node : edge)
      {
         if (node >= param.materials.size())
         {
            throw std::invalid_argument("Corrupt edge data: Node " + std::to_string(node) + " is not in the material graph!");
         }
      }
   }

   // weighted waves

   for (size_t i = 0; i < wavelens.size(); ++i)
   {
      param.waves.push_back({wavelens[i], weights[i]});
   }

   // print input data

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "incident field (phi,theta,psi, E0x,E0y):\n{\n";

      for (auto& e : param.E)
      {
         std::cout << "  " << e << "\n";
      }

      std::cout << "}\n\nwavelengths:\n{\n";

      for (auto& w : wavelens)
      {
         std::cout << "  " << w;
      }

      std::cout << "\n}\n\noptimization weights:\n{\n";

      for (auto& w : weights)
      {
         std::cout << "  " << w;
      }

      std::cout << "\n}\n\nmaterials:\n{\n";

      for (int idx = 0; idx < param.materials.size(); ++idx)
      {
         std::cout << "  " << idx << ":";

         for (auto& m : param.materials[idx])
            std::cout << "  " << m;

         std::cout << std::endl;
      }

      std::cout << "}\n\nedges:\n{\n";

      for (int idx = 0; idx < param.edges.size(); ++idx)
      {
         std::cout << "  " << idx << ":  [";

         auto& e = param.edges[idx];

         for (int j = 0; j < e.size(); ++j)
         {
            if (j > 0)
               std::cout << ", ";

            std::cout << e[j];
         }

         std::cout << "]\n";
      }

      std::cout << "}\n\n";
   }
}

void domain_decomposition(const InputArgs& args, Parameters& param)
{
   sleep(10);

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> DOMAIN DECOMPOSITION <<<<<<<<<<\n\n";
   }

   sgp::mpi::NP np_min(args.np_dof[0], args.np_wav[0], args.np_E[0]);
   sgp::mpi::NP np_max(args.np_dof[1], args.np_wav[1], args.np_E[1]);
   const double fuzzy_strat = 0.25;

   auto np = sgp::mpi::distribute(MPI_COMM_WORLD,
                                  args.N_xyz[2],
                                  param.waves.size(),
                                  param.E.size(),
                                  sgp::mpi::NP_dof::MAXIMIZE,
                                  fuzzy_strat,
                                  np_min,
                                  np_max);

   param.dd = sgp::mpi::DomainDecomposition(MPI_COMM_WORLD, np);

   if (args.verbose && param.rank == 0)
   {
      std::cout << "Total number of processes: " << param.dd.world.np << "\n";
      std::cout << "  " << param.waves.size() << " wavelengths, parallelized with np=" << param.dd.mod.np_1 << "\n";
      std::cout << "  " << param.E.size() << " incident direction/polarization tuples, parallelized with np=" << param.dd.mod.np_2
                << "\n";
      std::cout << "  " << args.N_xyz[2] << " dipole planes, parallelized with np=" << param.dd.dof.np << "\n";
      std::cout << "  -> " << param.dd.mod.np_1 << "x" << param.dd.mod.np_2 << "x" << param.dd.dof.np << " = "
                << (param.dd.mod.np_1 * param.dd.mod.np_2 * param.dd.dof.np) << " processes\n";
   }
}

void initialize_dda(const InputArgs& args, Parameters& param)
{
   sleep(10);

   sgp::Timer timer(param.dd.world.comm);

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> INIT DDA <<<<<<<<<<\n\n";
   }

   timer.start();

   dcomplex E0[2] = {{1, 0}, {0, 0}};

   std::vector<const char*> solvers{DDA_CG,
                                    DDA_CG_S,
                                    DDA_BICG,
                                    DDA_BICG_SYM,
                                    DDA_BICG_STAB,
                                    DDA_ML_BICG_STAB_ORG,
                                    DDA_ML_BICG_STAB,
                                    DDA_ML_BICG_STAB_SS,
                                    DDA_R_BICG_STAB,
                                    DDA_QMR,
                                    DDA_QMR_SYM,
                                    DDA_TF_QMR};

   int n_starting_vectors = (5 <= args.solver_idx && args.solver_idx <= 8) ? args.starting_vec : 0;

   dda_init(param.dd.dof.comm,
            solvers[args.solver_idx],
            args.precond,
            n_starting_vectors,
            args.scattering.data(),
            1,
            E0,
            args.shape.c_str(),
            args.N_xyz.data(),
            args.d,
            0);

   dda_configure_iterative_solver(args.dda_tol, 1e-50, args.dda_iter, 0);

   timer.stop();

   if (args.verbose && param.rank == 0)
   {
      std::cout << "Number of dipoles: " << dda_N_global() << "\n\n";
      std::cout << "Time for initializing DDA: " << timer.time_total() << " s\n";
   }
}

void compute_coating_layer(const InputArgs& args, Parameters& param)
{
   const int  N      = dda_N_local();
   const int* coords = dda_get_coordinates();

   param.coating_layer = std::vector<bool>(N, true);

   if (args.coating <= 0) // optimize all dipoles
   {
      return;
   }

   SHAPE shape;

   if (args.shape.compare(DDA_ELLIPSOID) == 0)
   {
      shape = SHAPE::ELLIPSOID;
   }
   else if (args.shape.compare(DDA_CUBOID) == 0)
   {
      shape = SHAPE::CUBOID;
   }
   else
   {
      throw std::invalid_argument("INPUT ERROR: Coating only available for standard shapes!");
   }

   // center coordinates
   double x0 = (param.domain[0] - 1) * args.d / 2.0;
   double y0 = (param.domain[1] - 1) * args.d / 2.0;
   double z0 = (param.domain[2] - 1) * args.d / 2.0;

   // semi-axis of core
   double a = param.domain[0] * args.d / 2.0 - args.coating;
   double b = param.domain[1] * args.d / 2.0 - args.coating;
   double c = param.domain[2] * args.d / 2.0 - args.coating;

   for (size_t i = 0; i < N; ++i)
   {
      // normalized coordinates: xn = (x - x0)/a
      double xn = (coords[3 * i + 0] * args.d - x0) / a;
      double yn = (coords[3 * i + 1] * args.d - y0) / b;
      double zn = (coords[3 * i + 2] * args.d - z0) / c;

      double r;

      if (shape == ELLIPSOID) // r = ||xn||_2^2
         r = xn * xn + yn * yn + zn * zn;

      if (shape == CUBOID) // r = ||xn||_max
         r = std::max({std::abs(xn), std::abs(yn), std::abs(zn)});

      param.coating_layer[i] = (r > 1.0);
   }
}

int n_stencil_entries(double filter)
{
   int se  = 0;
   int lim = int(std::sqrt(filter));

   for (int i = -lim; i <= lim; ++i)
   {
      for (int j = -lim; j <= lim; ++j)
      {
         for (int k = -lim; k <= lim; ++k)
         {
            double value = i * i + j * j + k * k;
            if (value < filter)
            {
               ++se;
            }
         }
      }
   }

   return se;
}

std::shared_ptr<PETScMatrix> gen_filtermatrix(const sgp::mpi::DomainDecomposition& dd,
                                              const vtk::Point3D<size_t>&          domain,
                                              std::vector<bool>                    coating_layer,
                                              double                               filter_val,
                                              bool                                 verbose)
{
   if (filter_val <= 1)
   {
      return nullptr;
   }

   int nonzeros_per_row = n_stencil_entries(filter_val);
   if (verbose && (dd.world.rnk == 0))
   {
      std::cout << "stencil size of filter matrix = " << nonzeros_per_row << "\n";
   }

   const int n = dda_N_local();
   const int N = dda_N_global();

   PETScMatrix F(n, N, nonzeros_per_row, "F", dd.dof.comm);
   PETScVector invRowSum(n, N, dd.dof.comm);

   // === extract dipole coordinates ===

   // std::cout << "extract dipole coordinates\n";

   const int* coords = dda_get_coordinates();

   PETScVector x_distr(n, N, dd.dof.comm);
   PETScVector y_distr(n, N, dd.dof.comm);
   PETScVector z_distr(n, N, dd.dof.comm);

   for (int i = 0; i < n; ++i)
   {
      x_distr.setValue_loc(i, coords[3 * i + 0]);
      y_distr.setValue_loc(i, coords[3 * i + 1]);
      z_distr.setValue_loc(i, coords[3 * i + 2]);
   }

   // we need to copy the values to all processors since getValue() only works for local data
   auto x_all = x_distr.gather();
   auto y_all = y_distr.gather();
   auto z_all = z_distr.gather();

   // for performance reasons, we acces the underlying arrays directly rather than via PETSc's interface
   auto x = x_all->get_data();
   auto y = y_all->get_data();
   auto z = z_all->get_data();

   // === copy all values of coating layer to each processor
   /* For particles i that are not part of the coating layer,
         F_ij = δ_ij    for all j.
      Since this also means
         F_ji = 0    for all j!=i,
      we need acces to the coating-flag of each dipole i,
      when assembling row j.
   */
   PETScVector coating_distr(n, N, dd.dof.comm);
   for (int i = 0; i < n; ++i)
   {
      complex_t val = (coating_layer[i]) ? 1.0 : 0.0;
      coating_distr.setValue_loc(i, val);
   }
   auto coating_all = coating_distr.gather();
   auto coating     = coating_all->get_data();

   // === setup matrix ===

   // std::cout << "setup matrix\n";
   // sgp::Timer timer_compute1(dd.world.comm, false);
   // sgp::Timer timer_compute2(dd.world.comm, false);
   // sgp::Timer timer_insert_matrix(dd.world.comm, false);
   // sgp::Timer timer_insert_vector(dd.world.comm, false);

   // for (int row = x_distr.idx0(); row < n + x_distr.idx0(); ++row)

   for (int r_loc = 0; r_loc < n; ++r_loc)
   {
      int row = r_loc + x_distr.idx0(); // global index

      real_t rowsum = 0;
      // real_t xr = x_all->getValue(row).real();
      // real_t yr = y_all->getValue(row).real();
      // real_t zr = z_all->getValue(row).real();
      real_t xr = x[row].real();
      real_t yr = y[row].real();
      real_t zr = z[row].real();

      if (not coating_layer[r_loc])
      {
         //for the constant particles we don't need filtering, i.e. F=id
         F.setValue(row, row, 1.0);
         invRowSum.setValue(row, 1.0);
      }
      else
      {
         for (int col = 0; col < N; ++col)
         {
            if (coating[col] != 0.0) // only coating particles are considered for filtering
            {
               // timer_compute1.start();
               // real_t dx = x_all->getValue(col).real() - xr;
               // real_t dy = y_all->getValue(col).real() - yr;
               // real_t dz = z_all->getValue(col).real() - zr;
               real_t dx = x[col].real() - xr;
               real_t dy = y[col].real() - yr;
               real_t dz = z[col].real() - zr;
               // timer_compute1.stop();

               // timer_compute2.start();
               auto dr2 = dx * dx + dy * dy + dz * dz;
               // timer_compute2.stop();

               if (dr2 < filter_val)
               {
                  // timer_insert_matrix.start();
                  real_t F_rc = filter_val - dr2;
                  F.setValue(row, col, F_rc);
                  rowsum += F_rc;
                  // timer_insert_matrix.stop();
               }
            }
         }
         // timer_insert_vector.start();
         invRowSum.setValue(row, 1.0 / rowsum);
         // timer_insert_vector.stop();
      }
   }

   // std::cout << "Time for\n"
   //          << "  computation1: " << timer_compute1.time_total() << "\n"
   //          << "  computation2: " << timer_compute2.time_total() << "\n"
   //          << "  inserting mat values: " << timer_insert_matrix.time_total() << "\n"
   //          << "  inserting vec values: " << timer_insert_vector.time_total() << "\n";

   // std::cout << "scale matrix\n";

   x_all->return_data(&x);
   y_all->return_data(&y);
   z_all->return_data(&z);
   coating_all->return_data(&coating);

   // normalize F row-wise
   return F.scaleDiag(invRowSum);
}

std::shared_ptr<PETScMatrix> handle_filter_matrix(const InputArgs& args, Parameters& param)
{
   sleep(10);

   sgp::Timer timer(param.dd.world.comm);

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> ASSEMBLE FILTER MATRIX <<<<<<<<<<\n\n";
   }

   std::shared_ptr<PETScMatrix> F;

   timer.start();

   if (args.load_F.empty())
   {
      F = gen_filtermatrix(param.dd, param.domain, param.coating_layer, args.filter_val, args.verbose);
   }
   else
   {
      F = std::make_shared<PETScMatrix>(param.dd.dof.comm, args.load_F);
   }

   timer.stop();

   if (args.verbose && param.rank == 0)
   {
      std::cout << "\nTime for assembly of filter matrix: " << timer.time_section() << " s\n";
   }

   if (not args.store_F.empty())
   {
      timer.new_section(true);

      if (param.dd.mod.rnk == 0)
      {
         F->print(args.store_F, true);
      }

      timer.stop();

      if (args.verbose && param.rank == 0)
      {
         std::cout << "\nTime for storing filter matrix: " << timer.time_section() << " s\n";
      }
   }

   return F;
}

std::vector<size_t> material_configuration(const std::vector<bool>& flag, size_t u, size_t v)
{
   size_t              N = flag.size();
   std::vector<size_t> U(N);
   for (size_t i = 0; i < N; ++i)
   {
      U[i] = (flag[i]) ? u : v;
   }
   return U;
}

template <int INTERPOLATION>
std::shared_ptr<SGP_DDA<INTERPOLATION>> initialize_optimization(const InputArgs& args, Parameters& param)
{
   // compute filter matrix
   bool filtering = (args.filter_val > 1 or (not args.load_F.empty()));

   auto F = (filtering) ? handle_filter_matrix(args, param) : nullptr;

   sleep(10);

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\n>>>>>>>>>> INIT SGP <<<<<<<<<<\n\n";
   }

   sgp::Timer timer(param.dd.world.comm);
   timer.start();

   // convert vectors to arrays
   sgp::Arr1D<typename SGP_DDA<INTERPOLATION>::edge_t> edges(param.edges.size());

   for (size_t i = 0; i < param.edges.size(); ++i)
   {
      if (param.edges[i].size() != INTERPOLATION + 1)
      {
         throw std::invalid_argument("INPUT ERROR: Each edge must consist of exactly " + std::to_string(INTERPOLATION + 1) +
                                     " nodes!");
      }

      std::copy(param.edges[i].begin(), param.edges[i].end(), edges[i].begin());
   }

   // configure sgp-algorithm
   auto alg = std::make_shared<SGP_DDA<INTERPOLATION>>(param.coating_layer,
                                                       param.materials,
                                                       edges,
                                                       param.waves,
                                                       param.E,
                                                       param.dd,
                                                       F,
                                                       args.filter_pen,
                                                       args.verbose && (param.rank == 0),
                                                       args.initGuessPold);

   alg->setParams(args.stopping_crit, args.tol, args.n_iter_outer, 0, args.n_iter_inner, args.eps_rho, args.eps_s, args.n_sec);

   timer.stop();

   // memory usage info

   if (args.verbose)
   {
      sleep(100);

      if (param.rank == 0)
      {
         std::cout << "Memory usage:\n";
      }

      sleep(10);

      for (int rnk = 0; rnk < param.np_world; ++rnk)
      {
         if (param.rank == rnk)
         {
            double dda_mem = dda_local_memory_usage() * 1E-6;
            double sgp_mem = alg->local_memory_usage() * 1E-6;
            std::cout.precision(3);
            std::cout << "  Rank " << rnk << ": " << sgp_mem << " (SGP) + " << dda_mem << " (DDA) = " << sgp_mem + dda_mem
                      << " MB" << std::endl;
         }
         MPI_Barrier(param.dd.world.comm);
      }
   }

   sleep(10);

   // timing info

   if (args.verbose && (param.rank == 0))
   {
      std::cout << "\nTime for initializing SGP: " << timer.time_total() << " s\n";
   }

   return alg;
}