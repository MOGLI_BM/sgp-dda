/*
    Helper functions for communication between SGP and DDA

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#pragma once

#include <array>
#include <vector>
#include <complex>

// extern "C" {
#include "dda.h"
// }

// real number
using real_t = double;
// complex number
using complex_t = std::complex<real_t>;
// vector of complex numbers
using C_vec = std::vector<complex_t>;
// vector of complex numbers for OpenDDA
using C_vec_dda = std::vector<dcomplex>;
// C^2, C^3 vectors
using complex_2d = std::array<complex_t, 2>;
using complex_3d = std::array<complex_t, 3>;
// vector of C^3 vectors
using C3_vec = std::vector<complex_3d>;
// R^3 vectors
using real_3d    = std::array<real_t, 3>;

struct polarization_t
{
   real_3d    target_rotation;
   complex_2d jones_vector;
};

struct wave_t
{
   real_t wavelen; // actual wavelenght
   real_t weight;  // weight to scale the term corresponding to wavelen in sumJ() and gradSumJ()
};

std::ostream& operator<<(std::ostream& out, const polarization_t& E);
std::ostream& operator<<(std::ostream& out, const complex_3d& vec);

/* convert std::complex<double> to OpenDDA type dcomplex
*/
inline dcomplex to_dcomplex(const complex_t& z)
{
    return {z.real(), z.imag()};
}

/*  convert material parameters from complex_t (n,k) to dcomplex epsilon (used within OpenDDA)
    @param src  vector of [refractive index / extinction coefficient] - pairs
    @param dst  vector of permettivities
*/
void convert_material(const C_vec& src, C_vec_dda& dst);

/*  convert OpenDDA solution vector to C3_vec
    @param  src     pointer to OpenDDA solution
    @param  dst     vector where the solution shall be stored
*/
void convert_solution(const dcomplex* src, C3_vec& dst);

/*  convert C3_vec to OpenDDA solution vector
    @param  src     vector where the solution shall be stored
    @param  dst     pointer to OpenDDA solution
*/
void convert_solution(const C3_vec& src, dcomplex* dst);

/* compute factor for Clausius-Mossotti polarisability
    @param nk   complex refractive index n+ik
    @returns alpha_cm
*/
complex_t alpha(const complex_t& nk);