/*
    Write data to VTK file.

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/
#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <map>
#include <set>
#include <algorithm>


#include <mpi.h>

namespace vtk {

/* convert typeid to name of datatype
        @returns name of type T
    */
template <typename T>
constexpr inline std::string dtype()
{
    if (typeid(T) == typeid(int))
    {
        return "Int" + std::to_string(8*sizeof(int));
    }

    if (typeid(T) == typeid(long))
    {
        return "Int"  + std::to_string(8*sizeof(long));
    }

    if (typeid(T) == typeid(long long))
    {
        return "Int"  + std::to_string(8*sizeof(long long));
    }

    if (typeid(T) == typeid(unsigned int))
    {
        return "UInt" + std::to_string(8*sizeof(int));
    }

    if (typeid(T) == typeid(size_t))
    {
        return "UInt" + std::to_string(8*sizeof(size_t));
    }

    if (typeid(T) == typeid(double))
    {
        return "Float64";
    }

    if (typeid(T) == typeid(float))
    {
        return "Float32";
    }

    return typeid(T).name();
}

template <typename T>
class Point3D : public std::array<T, 3>
{
  public:
    Point3D() : std::array<T, 3>()
    {}
    Point3D(const T& x_, const T& y_, const T& z_) : std::array<T, 3> {x_, y_, z_}
    {}
    Point3D(const Point3D& p) : std::array<T, 3>(p)
    {}

    inline T& x() {return (*this)[0];}
    inline T& y() {return (*this)[1];}
    inline T& z() {return (*this)[2];}
    inline const T& x() const {return (*this)[0];}
    inline const T& y() const {return (*this)[1];}
    inline const T& z() const {return (*this)[2];}
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const Point3D<T>& point)
{
    out << point.x() << " " << point.y() << " " << point.z();
    return out;
}


template <typename T>
using DataMap = std::map<Point3D<size_t>, T>;

enum DATAFORMAT {NONE = 0, BINARY = 1, ASCII = 2};

class Writer
{
  public:
    /* Ctor
        @param filename     name of vtk file
        @param format       format of vtk-output (Ascii or binary)
        @param descr        description of file contents
        @param mpi_rank     rank of this MPI process
        @param mpi_np       number of MPI processes
        @param mpi_comm     MPI communicator
    */
    Writer(const std::string& filename, DATAFORMAT format, const std::string& descr, size_t mpi_rank, size_t mpi_np, const MPI_Comm mpi_comm)
        : filen_(filename), outputFormat_(format), myRank_(mpi_rank), np_(mpi_np), comm_(mpi_comm)
    {
        // create file
        if (myRank_ == 0)
        {
            std::ofstream vtk(filen_);
            vtk << "<VTKFile type=\"ImageData\"  version=\"0.1\">\n";
        }
    }

    ~Writer()
    {
        writeLine("</VTKFile>\n");
    }

  protected:

    class Base64Writer
    {
      public:

        template <typename T>
        Base64Writer& operator<<(const T& data)
        {
            const char* bytePointer = reinterpret_cast<const char*>(&data);
            buffer_.insert(buffer_.end(), bytePointer, bytePointer + sizeof(T));
            return *this;
        }

        template <typename T>
        void init(size_t n_entries)
        {
            const uint32_t bytes = n_entries*sizeof(T);
            buffer_.clear();
            buffer_.insert(buffer_.begin(),
                                  reinterpret_cast<const char*>(&bytes),
                                  reinterpret_cast<const char*>(&bytes) + sizeof(uint32_t));
        }

        friend std::ostream& operator<<(std::ostream& os, Base64Writer& writer)
        {
            unsigned char input[3];
            unsigned char output[4];

            for (uint32_t i = 0; i < writer.buffer_.size(); i += 3)
            {
                if (writer.buffer_.size() - i < 3)
                {
                    const uint32_t length = writer.buffer_.size() - i;

                    for (uint32_t j = 0; j < length; ++j)
                        input[j] = static_cast<unsigned char>(writer.buffer_[i + j]);

                    for (uint32_t j = length; j < 3; ++j)
                        input[j] = static_cast<unsigned char>(0);

                    writer.encodeblock(input, output, int(length));
                    os << output[0] << output[1] << output[2] << output[3];
                }
                else
                {
                    input[0] = static_cast<unsigned char>(writer.buffer_[ i ]);
                    input[1] = static_cast<unsigned char>(writer.buffer_[i + 1]);
                    input[2] = static_cast<unsigned char>(writer.buffer_[i + 2]);
                    writer.encodeblock(input, output, 3);
                    os << output[0] << output[1] << output[2] << output[3];
                }
            }

            writer.buffer_.clear();

            return os;
        }


      private:

        void encodeblock(unsigned char in[3], unsigned char out[4], int len)
        {
            static const unsigned char cb64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

            switch (len)
            {
                case 1:
                    out[0] = cb64[ in[0] >> 2 ];
                    out[1] = cb64[((in[0] & 0x03) << 4) ];
                    out[2] = '=';
                    out[3] = '=';
                    break;

                case 2:
                    out[0] = cb64[ in[0] >> 2 ];
                    out[1] = cb64[((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4) ];
                    out[2] = cb64[((in[1] & 0x0f) << 2) ];
                    out[3] = '=';
                    break;

                default:
                    out[0] = cb64[ in[0] >> 2 ];
                    out[1] = cb64[((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4) ];
                    out[2] = cb64[((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6) ];
                    out[3] = cb64[ in[2] & 0x3f ];
            }
        }

        std::vector<char> buffer_;

    }; // class Base64Writer

    class AsciiWriter : public std::ostringstream
    {
      public:

        friend std::ostream& operator<<(std::ostream& os, AsciiWriter& writer)
        {
            os << writer.str();
            writer.str("");
            return os;
        }
    };

    void writeLine(const std::string& line) const
    {
        if (myRank_ == 0)
        {
            // append to file
            std::ofstream vtk(filen_, std::ios_base::app);

            // line
            vtk << line;
        }
    }

    template <typename T>
    Writer& operator<<(const T& dat)
    {
        if (outputFormat_ == BINARY && typeid(T) == typeid(double))
        {
            outputBinary_ << dat;
        }
        else //(outputFormat_ == ASCII)
        {
            outputAscii_ << dat << " ";
        }

        return *this;
    }

    template <typename T>
    void appendDataToVTK()
    {
        std::ofstream vtk(filen_, std::ios_base::app);

        if (outputFormat_ == BINARY && typeid(T) == typeid(double))
        {
            vtk << outputBinary_;
        }
        else //(outputFormat_ == ASCII)
        {
            vtk << outputAscii_;
        }
    }

    const std::string   filen_;

    const size_t        myRank_;
    const size_t        np_;
    const MPI_Comm      comm_;

    AsciiWriter         outputAscii_;
    Base64Writer        outputBinary_;
    const DATAFORMAT    outputFormat_;
};


class StructuredWriter : public Writer
{
  public:

    /* Ctor
        @param filename     name of vtk file
        @param format       format of vtk-output (Ascii or binary)
        @param descr        description of file contents
        @param mpi_rank     rank of this MPI process
        @param mpi_np       number of MPI processes
        @param mpi_comm     MPI communicator
        @param N            number of points in x,y,z direction
        @param h            mesh width in x,y,z direction
        @param origin       origin of the mesh, i.e., x_min,y_min,z_min
        @param planes       z-planes with data stored on this process
    */
    StructuredWriter(const std::string& filename, DATAFORMAT format, const std::string& descr,
                     size_t mpi_rank, size_t mpi_np, const MPI_Comm mpi_comm,
                     const Point3D<size_t>& N, const Point3D<double>& h, const Point3D<double>& origin, const std::set<size_t>& planes)
        : Writer(filename, format, descr, mpi_rank, mpi_np, mpi_comm)
        , N_(N)
        , planes_(planes)
    {
        if (myRank_ == 0)
        {
            std::ofstream vtk(filen_, std::ios_base::app);
            std::stringstream extend;
            extend << 0 << " " << (N.x()-1) << " " << 0 << " " << (N.y()-1) << " " << 0 << " " << (N.z()-1);

            vtk << " <ImageData WholeExtent=\"" << extend.str() << "\""
                                << " Origin=\"" << origin       << "\""
                                << " Spacing=\"" << h           << "\">\n";
            vtk << "  <Piece Extent=\"" << extend.str() << "\">\n";
            vtk << "   <PointData>\n";
        }
    }

    ~StructuredWriter()
    {
        writeLine("   </PointData>\n  </Piece>\n </ImageData>\n");
    }


    // todo add support for vectorial data
    /* add data
        @param name     name of data field
        @param data     vectorial data
        @param deflt    default value, used for empty fields in the grid
    */
    // template <typename T>
    // void add_data_vectors(const std::string& name, const DataMap<Point3D<T>>& data, const Point3D<T>& deflt)
    // {
    //     std::string head = "VECTORS " + name + " " + dtype<T>() + "\nLOOKUP_TABLE default\n";
    //     writeHeader(head);
    //     writeData<Point3D<T>>(data, deflt);
    //     writeHeader("");
    // }

    /* add data
        @param name     name of data field
        @param data     scalar data
        @param deflt    default value, used for empty fields in the grid
    */
    template <typename T>
    void add_data_scalar(const std::string& name, const DataMap<T>& data, const T& deflt)
    {
        std::stringstream head;
        head << "    <DataArray type=\"" << dtype<T>() << "\" "
                        << "Name=\"" << name       << "\" "
                        << "NumberOfComponents=\"1\" ";

        if (outputFormat_ == BINARY && typeid(T) == typeid(double))
        {
            head << "format=\"binary\">\n     ";
        }
        else //(outputFormat_ == ASCII)
        {
            head << "format=\"ascii\">\n     ";
        }

        writeLine(head.str());
        writeData<T>(data, deflt);
        writeLine("    </DataArray>\n");
    }

  private:

    /* write data to vtk file
        here, we assume that the domain is distributed over the MPI processes along the
        z-axis, i.e., each z-plane is stored entirely on one process

        @param data     map containing the data
        @param non      value to insert if a point doesn't contain any data
    */
    template <class DAT>
    void writeData(const DataMap<DAT>& data, const DAT& none)
    {
        for (size_t z = 0; z < N_.z(); ++z)
        {
            // check whether plane z is located on this process
            if (std::find(planes_.begin(), planes_.end(), z) != planes_.end())
            {
                if (z == 0) outputBinary_.init<DAT>(N_.x() * N_.y() * N_.z());

                for (size_t y = 0; y < N_.y(); ++y)
                {
                    for (size_t x = 0; x < N_.x(); ++x)
                    {
                        auto d = data.find({x, y, z});

                        if (d != data.end())    *this << d->second;
                        else                    *this << none;
                    }
                }

                appendDataToVTK<DAT>();
            }

            MPI_Barrier(comm_);
        }
        writeLine("\n");
    }

    const Point3D<size_t>       N_;
    const std::set<size_t>   planes_;
};

} // namespace vtk
