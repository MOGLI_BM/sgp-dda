# Multi Material Optimization based on Discrete Dipole Approximation and Sequential Global Programming

## Build instructions

#### Requirements
* CMake
* MPI
* PETSc (with complex arithmetic)
* FFTW3

To build SGP-DDA, clone the source code:

    $ git clone --recurse-submodules https://gitlab.com/MOGLI_BM/sgp-dda.git

`--recurse-submodules` will automatically initialize and clone SGP, OpenDDA-lib and TyFiR as submodules.
Create a build-directory and run cmake and ccmake:

    $ mkdir build
    $ cd build
    $ cmake ..
    $ ccmake .

Add/adjust the following variables:
```
 LIB_SEARCH_DIR: path where your libraries are stored (required if FFTW3 has not been found)
 PETSC_DIR:      path to your petsc installation, i.e., a directory containing the includes and library
 PETSC_ARCH:     name of chosen (complex) PETSc installation
```

You can now compile and run the program with the provided example input-data files:

    $ make
    $ bin/DDA_Opt ../data/config.example

Or, for a parallel run:

    $ mpirun -n 4 bin/DDA_Opt ../data/config.example

## Documentation

In general, the algorithm can be used to optimize the material composition of a particle to optimize the extinction for certain incident fields.
For a more thorough description of the algorithm, see [DDA_SGP.pdf](https://gitlab.com/MOGLI_BM/sgp-dda/-/blob/master/DDA_SGP.pdf).

The parameters of the simulation and optimization are controlled by four input files:

### Config
The main parameter file for controlling the shape and size of the simulation domain, mesh width, number of iterations and so on and so forth.
For a detailed description of all the parameters, we refer to [config.example](https://gitlab.com/MOGLI_BM/sgp-dda/-/blob/master/data/config.example).

### Material description
This [file](https://gitlab.com/MOGLI_BM/sgp-dda/-/blob/master/data/materials.example) contains all the required material parameters in the following fashion:
* first row: list of all wavelengths that shall considered for the optimization
* second row: list of weights corresponding to each wavelength.\
    positive weight -> extinction for this wavelength shall be minimized\
    negative weight -> extinction for this wavelength shall be maximized
* each i-th row thereafter: material parameters (n,k) of material i for the corresponding wavelength\
    (n = refractive index, k = extinction coefficient)

The path to this file has to be specified in the config-file as `material_bib`.

### Material graph
The graph of all materials which are admissible optimization targets is specified via its [edges](https://gitlab.com/MOGLI_BM/sgp-dda/-/blob/master/data/edges.example):
Each row consists of `n_en` material-IDs corresponding to a material in `material_bib`,
where `n_en` describes the type of interpolation scheme which is used on the edges.
Currently only linear interpolation, i.e., `n_en = 2` is supported.

Note that it is not necessary to connect all materials by edges.
This way it is possible to define materials for e.g. starting conditions, which are not supposed to be part of the optimization result.

The path to this file has to be specified in the config-file as `material_graph`.

### Incident field
The [incident direction](https://gitlab.com/MOGLI_BM/sgp-dda/-/blob/master/data/incident_field.example) and polarization are specified as follows:
The direction is always assumed to be `[0,0,1]` and then changed by rotating the target.
Each row in this file contains 3 real and 2 complex values given by:
`a b c (x_r x_i) (y_r y_i)`
where `a`, `b`, `c` are the Euler angles defining the target rotation and
`(x_r x_i)`, `(y_r y_i)` are the `x` and `y` coordinate of the polarization, respectively (w.r.t. to the unrotated system).

For a detailed description, we refer to James Mc Donald's thesis, "OpenDDA - A Novel High-Performance Computational Framework for the Discrete Dipole Approximation".

The path to this file has to be specified in the config-file as `incident_params`.

## Development

When adding features to this framework, one should make sure, that the program still behaves as expected.
For this purpose, a small test suite has been added.

To run all tests go to the root directory, then:
```
$ cd tests
$ python3 runtest.py PATH_TO_BUILD_DIR
```
You should only submit a pull request once all test have been run successfully!

When adding new tests, make sure that the naming of files and structure of directories matches the existing tests.
